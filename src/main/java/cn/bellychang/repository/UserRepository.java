package cn.bellychang.repository;

import cn.bellychang.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ChangLiang
 * @date 2018/4/10
 */
public interface UserRepository extends JpaRepository<User,Long> {
}
