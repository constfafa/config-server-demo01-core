config-server配置服务--客户端（core服务）部署步骤：

1.添加依赖：compile('org.springframework.cloud:spring-cloud-starter-config')

2.将项目中的application.properties转移到配置中心（或git相应分支）中，如core.properties

3.添加bootstrap.yml文件，配置内容如下： spring: application: name: core cloud: config: uri: http://localhost:8888 name: core fail-fast: true password: root username: user

#其中： spring.cloud.config.name为被工程名称，需与配置中心（或git）相应的配置文件properties的名称保存一致 spring.cloud.config.uri= http://localhost:8888/ 指明配置服务中心的网址。